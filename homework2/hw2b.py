
"""
Demonstration module for quadratic interpolation.
Update this docstring to describe your code.
Modified by: ** Nuan **
"""


import numpy as np
import matplotlib.pyplot as plt
from numpy.linalg import solve

def quad_interp(xi,yi):
    """
    Quadratic interpolation.  Compute the coefficients of the polynomial
    interpolating the points (xi[i],yi[i]) for i = 0,1,2.
    Returns c, an array containing the coefficients of
      p(x) = c[0] + c[1]*x + c[2]*x**2.

    """

    # check inputs and print error message if not valid:

    error_message = "xi and yi should have type numpy.ndarray"
    assert (type(xi) is np.ndarray) and (type(yi) is np.ndarray), error_message

    error_message = "xi and yi should have length 3"
    assert len(xi)==3 and len(yi)==3, error_message

    # Set up linear system to interpolate through data points:
    #A = np.array([[1, xi[0], xi[0]**2], [1, xi[1], xi[1]**2], [1, xi[2], xi[2]**2]])
    A = np.vstack([np.ones(3), xi, xi**2]).T
    b = yi

    ### Fill in this part to compute c ###
    c = solve(A, b)

    return c
    
def cubic_interp(xi, yi):
    '''
    Cubic interpolation. Compute the coefficients of the polynomial interpolating the points
    (xi[i], yi[i]) for i = 0,1,2,3. Returns c, an array containing coefficients of
        p(x) = c[0] + c[1]*x + c[2]*x**2 + c[3]*x**3
    '''
    
    #check inputs and print error message if not valid:
    
    error_message = "xi and yi should have type numpy.ndarray"
    assert(type(xi) is np.ndarray) and (type(yi) is np.ndarray), error_message
    
    error_message = "xi and yi should have length 3"
    assert len(xi) == 4 and len(yi) == 4, error_message
    
    
    A = np.vstack([np.ones(4), xi, xi**2, xi**3]).T
    b = yi
     
    c = solve(A, b)
    
    return c
    
def poly_interp(xi, yi):
    '''
    Polynomial interpolation. Compute the coefficients of the polynomial intepolating the points
    (x[i], y[i]) for i = 0,1,2...n. Returns c, an array containing coefficients of
            p(x) = c[0] + c[1]*x + c[2]*x**2 + ..... + c[n]*x**n
    '''
    #check inputs and print error message if not valid:
    
    error_message = "xi and yi should have type numpy.ndarray"
    assert (type(xi) is np.ndarray) and (type(yi) is np.ndarray), error_message
    
    error_message = "xi and yi should be of same length"
    assert len(xi) == len(yi), error_message
    
    A = np.ndarray(shape = (len(xi), len(xi)), dtype = float)
    
    
    for i in range(len(xi)):
        for j in range(len(xi)):
            A[i][j] = xi[i]**j
            
    b = yi
    
    c = solve(A, b)
    
    return c
        
    
    
    
def plot_quad(xi, yi):
    '''
    Calls the quad_interp(xi, yi) to compute c and plots the interpolating polynomial and data points
    and saves the resulting figure as quadratic.png.
    '''
    
    c = quad_interp(xi, yi)
    
    #Plot c
    x = np.linspace( xi.min() - 1, xi.max() + 1, 1000)
    y = c[0] + c[1] * x + c[2] * (x**2)
    
    plt.figure(1)
    plt.clf()
    plt.plot(x, y, 'b-')
    
    plt.plot(xi, yi, 'ro')
    plt.ylim(-2, 8)
    
    plt.title("Data Points and Interpolating polynomial")
    
    plt.savefig('hw2b.png')
    
def plot_cubic(xi, yi):
    '''
    Calls and plots the result from cubic_interp and saves the resulting figure as quadratic.png
    '''
    
    c = cubic_interp(xi, yi)
    
    x = np.linspace( xi.min() - 1, xi.max() + 1, 1000)
    y = c[0] + c[1]*x + c[2]*x**2 + c[3]*x**3
    
    plt.figure(1)
    plt.clf()
    plt.plot(x, y, 'b-')
    
    plt.plot(xi, yi, 'ro')
    plt.ylim(-2, 8)
    
    plt.title(" Data Points and interpolating Cubic polynomial")
    
    plt.savefig('cubic.png')
    
def plot_poly(xi, yi):
    '''
    Calls and plots the result from poly_interp and saves the resulting figure as poly.png
    '''
    c = poly_interp(xi, yi)
    
    x = np.linspace(xi.min() - 1, xi.max() + 1, 1000 )
    y = 0
    for i in range(len(xi)):
        y = y + c[i]*x**i
    
    plt.figure(1)
    plt.clf
    plt.plot(x, y, 'b-')
    
    plt.plot(xi, yi, 'ro')
    plt.ylim(-2, 8)
    
    plt.title(" Data Points and interpolating n degree polynomial")
    plt.savefig('poly.png')
    


def test_quad1():
    """
    Test code, no return value or exception if test runs properly.
    """
    xi = np.array([-1.,  0.,  2.])
    yi = np.array([ 1., -1.,  7.])
    c = quad_interp(xi,yi)
    c_true = np.array([-1.,  0.,  2.])
    print "c =      ", c
    print "c_true = ", c_true
    # test that all elements have small error:
    assert np.allclose(c, c_true), \
        "Incorrect result, c = %s, Expected: c = %s" % (c,c_true)
        
def test_cubic1():
    '''
    Test code, no return value or exception if test runs properly.
    '''
    
    xi = np.array([1, 2, 3, 4])
    yi = np.array([3, 9, 27, 63])
    
    c = cubic_interp(xi, yi)
    c_true = np.array([3, -1, 0, 1])
    
    print "c(cubic1) =  ", c
    print "c_true(cubic1) = ", c_true
    
    #test that all elements have small error:
    assert np.allclose(c, c_true), "Incorrect result, c = %s, Expected: c = %s " % (c, c_true)
    
def test_poly1():
    '''
    Test code, no return value or exception if test runs properly 
    '''
    xi = np.array([1, 2, -1, 0, -2])
    yi = np.array([3, 39, -9, -5, 15])
    
    c = poly_interp(xi, yi)
    c_true = np.array([-5, 6, 0, 0, 2])
    
    print "c(poly1) = ", c
    print "c_true(poly1) = ", c_true
    
    assert np.allclose(c, c_true), "Incorrect result, c = %s, Expected: c = %s" %(c, c_true)
        
def test_poly2():
    '''
    Test code, no return value or exception if test runs properly 
    '''
    xi = np.array([0, 1, -1, 2, -2, 3])
    yi = np.array([-1, 1, -1, 47, -17, 323])
    
    c = poly_interp(xi, yi)
    c_true = np.array([-1, 0, 0, 0, 1, 1])
    
    print "c(poly2) = ", c
    print "c_true(poly2) = ", c_true
    
    assert np.allclose(c, c_true), "Incorrect result, c = %s, Expected: c = %s" %(c, c_true)

    
if __name__=="__main__":
    # "main program"
    # the code below is executed only if the module is executed at the command line,
    #    $ python demo2.py
    # or run from within Python, e.g. in IPython with
    #    In[ ]:  run demo2
    # not if the module is imported.
    print "Running test..."
    test_quad1()
    test_cubic1()
    test_poly1()
    test_poly2()

