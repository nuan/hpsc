
module functions

    use omp_lib
    implicit none
    integer :: fevals(0:7), gevals(0:7)
    real(kind=8) :: k
    save

contains

    real(kind=8) function f(x)
        implicit none
        real(kind=8), intent(in) :: x 
        integer :: thread_num, ny = 1000, i
	real(kind = 8) :: y, h, y_sum, a, b
	!real(kind = 8), external :: g

        ! keep track of number of function evaluations by
        ! each thread:
        thread_num = 0   ! serial mode
        !$ thread_num = omp_get_thread_num()
        fevals(thread_num) = fevals(thread_num) + 1

	! integrate g(x, y) with in the interval 1 to 4 using the trapezoidal rule

	a = 1.d0
	b = 4.d0
	h = (b - a)/(ny - 1)
	y = 1.d0
	y_sum = 0.5 * (g(x, a) + g(x, b))

	do i = 2, ny-1
		y = a + (i - 1) * h
		y_sum = y_sum + g(x, y)
		end do
       
        f = (h * y_sum)
        
    end function f

	real(kind = 8) function g(x, y)
		implicit none
		real(kind = 8), intent(in) :: x, y
		integer thread_num

		thread_num = 0
		!$ thread_num = omp_get_thread_num()
		gevals(thread_num) = gevals(thread_num) + 1

		! compute g(x, y)
		g = sin(x + y)
	end function g

end module functions
