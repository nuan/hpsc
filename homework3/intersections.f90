
program main
	! Program to find the intersection points of xcos(pi*x) and 1 - 0.6x^2	
	use functions
	use newton
	implicit none
	real(kind = 8) :: x0, x
	real(kind = 8), dimension(4) :: r 
	real(kind = 8), external :: fval, pval
	integer :: i, iters  
	logical :: debug

	debug = .false.
	r = (/-3., -2., -0.8e-0, 1.5/)

	do i = 1, 4
		x0 = r(i)
		call solve(fval, pval, x0, x, iters, debug)
	
		print *, 'With initial guess x0 =', x0
		print 10, x, iters
	10	format('solve returns x =', e22.15, ' after ', i0, ' iterations') 
	end do
	

end program main

real(kind = 8) function fval(x)
	use functions
	implicit none
	real(kind = 8), intent(in) :: x
	!real(kind = 8) :: f, g

	fval = f(x) - g(x)
end function fval

real(kind = 8) function pval(x)
	use functions
	implicit none
	real(kind = 8), intent(in) :: x
	!real(kind = 8) :: fprime, gprime

	pval = fprime(x) - gprime(x)
end function pval

