# -*- coding: utf-8 -*-
"""
Created on Wed Dec 18 10:00:23 2013

@author: pavan
"""

def solve(fvals, x0, debug = False):
    '''
    The function return the result of the final iterate (approximation
    of the root)
    '''
    x = x0
    
    if debug == True:
        print 'Initial guess: %f' %x
    maxiters = 20
    
    for i in range(maxiters):
        f, fp = fvals(x)
        
        if(abs(f) < 1e-14):
            break
        
        x = x - (f/fp)
        if debug == True:
            print 'After %i iterations, x = %22.15e ' %(i, x)
        
    return x, i
    
def fvals_sqrt(x):
    '''
    Return f(x) and f'(x) for applying newton to find a square root
    '''
    f = x**2 - 4
    fp = 2. * x 
    
    return (f, fp)

def test1(debug_solve = False):
    '''
    Test Newton iteration for the square root with different
    initial conditions
    '''
    from numpy import sqrt
    
    for x0 in [1., 2., 100.]:
        print " " #blank line
        x, iters = solve(fvals_sqrt, x0, debug=debug_solve)
        print "solve returns x = %22.15e after %i iterations" % (x, iters)
        fx, fpx = fvals_sqrt(x)
        print "The value of f(x) is %22.15e" %fx
        assert abs(x - 2.) < 1e-14, "***Unexpected result: x = %22.15e" %x
        
        
