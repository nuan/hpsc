# -*- coding: utf-8 -*-
"""
Created on Wed Dec 18 18:13:58 2013

@author: pavan
"""

from newton import solve
import numpy as np
import matplotlib.pylab as plt

def fvals(x):
    '''
    Return f(x) and f'(x)
    '''
    
    f = (x * np.cos(np.pi * x)) - (1 - (0.6*x*x))   #f(x) = xcos(pi*x) - 1 - (0.6 * x**2)
    fp = (np.cos(np.pi * x) - (x * np.sin(np.pi*x) * np.pi) +
         1.2 * x )

    return f, fp
    
def test2(debug_solve = False):
    
    for x0 in [-3., -2., -0.8, 1.5 ]:
        x, iters = solve(fvals, x0, debug_solve)
        
        print 'With initial guess x0 = %22.15e' %x0
        print 'solve returns x = %22.15e after %i iterations' %(x, iters)
        

if __name__ == "__main__":
    x = np.linspace(-10, 10, 1000) # [i for i in range(-10, 11)]
    y = [i * np.cos(np.pi * i) for i in x]
    z = [(1 - (0.6 * i**2)) for i in x]
    
    #plt.figure(1)
    plt.xlim(-5, 5)
    plt.ylim(-4, 4)
    plt.title("Intersections")
    plt.plot(x, y, label = 'g1')
    plt.plot(x, z, label= 'g2')
    
    for x0 in [-3., -2., -0.8, 1.5 ]:
        x, iters = solve(fvals, x0)
        plt.plot(x, x*np.cos(np.pi * x), 'ko')
    plt.legend()
    plt.savefig("intersections")
    plt.show()
    