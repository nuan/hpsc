! $UWHPSC/codes/fortran/newton/functions.f90

module functions

	use newton
	implicit none
	real(kind = 8) :: epsilon
	save

contains

real(kind=8) function f_sqrt(x)
    implicit none
    real(kind=8), intent(in) :: x

    f_sqrt = x**2 - 4.d0

end function f_sqrt


real(kind=8) function fprime_sqrt(x)
    implicit none
    real(kind=8), intent(in) :: x
    
    fprime_sqrt = 2.d0 * x

end function fprime_sqrt

	! Functions added by me 
real(kind = 8) function f(x)
	implicit none
	real(kind = 8), intent(in) :: x
	real(kind = 8), parameter :: pi  = acos(-1.d0)

	f = x * cos(pi*x)
end function f

real(kind = 8) function g(x)
	implicit none
	real(kind = 8), intent(in) :: x
	
	g = 1 - 0.6*x*x
end function g

real(kind = 8) function fprime(x)
	implicit none
	real(kind = 8), intent(in) :: x
	real(kind = 8), parameter :: pi = acos(-1.d0)

	fprime = cos(pi*x) - x * sin(pi*x) * pi
end function fprime

real(kind = 8) function gprime(x)
	implicit none
	real(kind = 8), intent(in) :: x

	gprime = - 1.2 * x
end function gprime

real(kind = 8) function f_quartic(x)
	implicit none
	real(kind = 8), intent(in) :: x

	f_quartic = (x - 1)**4 - epsilon
end function f_quartic

real(kind = 8) function fprime_quartic(x)
	implicit none
	real(kind = 8), intent(in) :: x

	fprime_quartic = 4*(x - 1)**3
end function fprime_quartic
 
end module functions
