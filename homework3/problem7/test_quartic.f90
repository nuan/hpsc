
program test_quartic
	use functions
	use newton
	implicit none
	real(kind = 8) :: x0, x, xstar
	real(kind = 8), dimension(3) :: eps, t
	integer :: i, j, iters
	logical :: debug = .false.

	x0 = 4.e-0
	print *, 'With initial guess ', x0

	print *, '    epsilon        tol    iters          x                 f(x)        x-xstar'
 
	eps = (/1.e-04, 1.e-08, 1.e-12/)
	t = (/1.e-05, 1.e-10, 1.e-14/)
	do i = 1, 3
		epsilon = eps(i)
		xstar = 1 + epsilon**(1/4d0)
		do j = 1, 3
			tol = t(j)
			call solve(f_quartic, fprime_quartic, x0, x, iters, debug)
			print 10, epsilon, tol, iters, x, f_quartic(x), x - xstar
		10	format(2es13.3, i4, es24.15, 2es13.3)
		end do
		print *,''
	end do
end program test_quartic
