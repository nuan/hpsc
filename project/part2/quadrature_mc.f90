module quadrature_mc

contains
	
	function quad_mc(g, a, b, ndim, npoints)
		implicit none
		real(kind = 8), external :: g
		integer, intent(in) :: ndim, npoints
		real(kind = 8), dimension(ndim), intent(in) :: a, b

		real(kind = 8), dimension(npoints, ndim) :: r
		real(kind = 8) :: g_sum, quad_mc
		integer :: i

		call random_number(r)
		g_sum = 0
		do i = 1, npoints
			r(i, :) = a + r(i, :)*(b-a)	! so that each random number is with in the range of the interval(a & b) 
			g_sum = g_sum + g(r(i, :), ndim)
			end do

		quad_mc = (product(b - a)/npoints) * g_sum 	! volume = product(b-a)

	end function quad_mc

end module quadrature_mc
