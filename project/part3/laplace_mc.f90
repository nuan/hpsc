program laplace_mc
	use mc_walk
	use problem_description
	use random_util
	implicit none
	real(kind = 8) :: i0, j0, x0, y0, u_true, u_mc, error, u_mc_total, u_sum_old, u_sum_new
	integer :: seed1, max_steps, n_mc, n_success, n_total, i

	open(unit=25, file='mc_laplace_error.txt', status='unknown')
	x0 = 0.9
	y0 = 0.6

	i0 = nint((x0 - ax)/dx)
	j0 = nint((y0 - ay)/dy)

	x0 = ax + i0*dx
	y0 = ay + j0*dy

	u_true = utrue(x0, y0)

	seed1 = 12345
	call init_random_seed(seed1)
	!print *, "seed1 for random number generator:	", seed1

	max_steps = 100*max(nx, ny)
	n_mc = 10

	u_mc_total = 0
	n_total = 0
	nwalks = 0

	do i = 1, 13
		u_sum_old = u_mc_total * n_total
		call many_walks(i0, j0, max_steps, n_mc, u_mc, n_success)
        	u_sum_new = u_mc * n_success
        	n_total = n_total + n_success
        	u_mc_total = (u_sum_old + u_sum_new) / n_total
        	error = abs((u_mc_total - u_true) / u_true)

		print 11, n_total, u_mc_total, error 
	11	format(i10," ", e23.15, e15.6)

		write(25, '(i10, e23.15, e15.6)') n_total, u_mc_total, error
		
		! Included the following to get the same output as in solution
		!During the first two iterations, n_mc is same
		if (i /= 1) then
			n_mc = 2*n_mc
			endif
		end do

	print 12, u_mc_total
12	format("Final approximation to u(x0, y0): ", es22.14)

	print *,"Total number of random walks:", nwalks

	

	
	
end program laplace_mc
