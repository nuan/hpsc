module mc_walk
	use problem_description
	implicit none
	integer :: nwalks
	
contains

subroutine random_walk(i0, j0, max_steps, ub, iabort)
	implicit none
	real(kind = 8), intent(in) :: i0, j0
	real(kind = 8), intent(inout) :: ub
	integer, intent(in) :: max_steps
	integer, intent(inout) :: iabort

	integer :: istep, i, j
	real(kind = 8), dimension(max_steps) :: r
	real(kind = 8) :: xb, yb


	call random_number(r)

	nwalks = nwalks + 1
	i = i0
	j = j0
	do istep = 1, max_steps
		if(r(istep) < 0.25) then
			i = i-1
			else if(r(istep) < 0.5) then
				i = i+1
				else if(r(istep) < 0.75) then
					j = j-1
					else
						j = j+1
			end if

		! Check if we hit the boundary
		if (i*j*(nx+1 - i)*(ny+1 - j) == 0) then
			xb = ax + i*dx
			yb = ay + j*dy

			ub = uboundary(xb, yb)
			iabort = 0
			exit
			end if

		end do
	if (istep == max_steps + 1) then
		ub = 1001	! can be anything
		iabort = 1
		endif


end subroutine random_walk

subroutine many_walks(i0, j0, max_steps, n_mc, u_mc, n_success)
	implicit none
	real(kind = 8), intent(in) :: i0, j0
	integer, intent(in) :: max_steps, n_mc
	integer, intent(out) :: n_success
	real(kind = 8), intent(out) :: u_mc

	integer :: mc, iabort
	real(kind = 8) ::  ub, ub_sum, i, j

	ub_sum = 0
	n_success = 0
	do mc = 1, n_mc
		call random_walk(i0, j0, max_steps, ub, iabort)

		if (iabort == 0) then
			ub_sum = ub_sum + ub
			n_success = n_success + 1
			end if
		end do

	u_mc = ub_sum / n_success
end subroutine many_walks	

end module mc_walk
