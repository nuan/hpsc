module mc_walk
	use problem_description
	implicit none
	integer :: nwalks
	
contains

subroutine random_walk(i0, j0, max_steps, ub, iabort)
	implicit none
	real(kind = 8), intent(in) :: i0, j0
	real(kind = 8), intent(inout) :: ub
	integer, intent(in) :: max_steps
	integer, intent(inout) :: iabort

	integer :: istep, i, j
	real(kind = 8), dimension(max_steps) :: r
	real(kind = 8) :: xb, yb


	call random_number(r)

	nwalks = nwalks + 1
	i = i0
	j = j0
	do istep = 1, max_steps
		if(r(istep) < 0.25) then
			i = i-1
			else if(r(istep) < 0.5) then
				i = i+1
				else if(r(istep) < 0.75) then
					j = j-1
					else
						j = j+1
			end if

		! Check if we hit the boundary
		if (i*j*(nx+1 - i)*(ny+1 - j) == 0) then
			xb = ax + i*dx
			yb = ay + j*dy

			ub = uboundary(xb, yb)
			iabort = 0
			exit
			end if

		end do
	if (istep == max_steps + 1) then
		ub = 1001	! can be anything
		iabort = 1
		endif


end subroutine random_walk

subroutine many_walks(i0, j0, max_steps, n_mc, u_mc, n_success)

	use mpi

	implicit none
	real(kind = 8), intent(in) :: i0, j0
	integer, intent(in) :: max_steps, n_mc
	integer, intent(out) :: n_success
	real(kind = 8), intent(out) :: u_mc

	integer :: mc, iabort, num_procs, proc_num, jobs, numsent, sender, j, ierr
	real(kind = 8) ::  ub, ub_sum, i
	integer, dimension(MPI_STATUS_SIZE) :: status

	call MPI_COMM_SIZE(MPI_COMM_WORLD, num_procs, ierr)
	call MPI_COMM_RANK(MPI_COMM_WORLD, proc_num, ierr)

	ub_sum = 0
	n_success = 0
	numsent = 0

	! Code for the master
	if (proc_num == 0) then
		jobs = min(n_mc, num_procs-1)

		do mc = 1, jobs
			call MPI_SEND(MPI_BOTTOM, 0, MPI_DOUBLE_PRECISION, mc, 1, &
                      	MPI_COMM_WORLD, ierr)
			numsent = numsent + 1
			end do 

		

		do mc = 1, n_mc
			call MPI_RECV(ub, 1, MPI_DOUBLE_PRECISION, &
                        MPI_ANY_SOURCE, MPI_ANY_TAG, &
                        MPI_COMM_WORLD, status, ierr)

			sender = status(MPI_SOURCE)
			j = status(MPI_TAG)

			if (j == 0) then
				ub_sum = ub_sum + ub
				n_success = n_success + 1
				endif
				
			if (numsent < n_mc) then
				call MPI_SEND(MPI_BOTTOM, 0, MPI_DOUBLE_PRECISION, sender, 1, &
                      		MPI_COMM_WORLD, ierr)
				numsent = numsent + 1
				
				else
				call MPI_SEND(MPI_BOTTOM, 0, MPI_DOUBLE_PRECISION, sender, 0, &
                      		MPI_COMM_WORLD, ierr)
				end if
			end do
		end if

		

	! Code for the worker
	if (proc_num /= 0) then
		if (proc_num > n_mc) goto 99
		
		do while( .true. )

			call MPI_RECV(MPI_BOTTOM, 0, MPI_DOUBLE_PRECISION, &
                      	0, MPI_ANY_TAG, &
                      	MPI_COMM_WORLD, status, ierr)

			j = status(MPI_TAG)

			if (j == 1) then
				call random_walk(i0, j0, max_steps, ub, iabort)
				
				call MPI_SEND(ub, 1, MPI_DOUBLE_PRECISION, 0, iabort, &
                      		MPI_COMM_WORLD, ierr)
				
				end if
			if (j == 0) goto 99

			end do


		end if
99	continue

	if (proc_num == 0) then	
		u_mc = ub_sum / n_success
		else
			u_mc = 0 ! some thing
			n_success=0
		end if

	call MPI_BARRIER(MPI_COMM_WORLD,ierr)
end subroutine many_walks	

end module mc_walk
