from pylab import *

n_total, u_mc_total, error = loadtxt("mc_laplace_error.txt", unpack=True)

figure(1)
clf()
loglog(n_total,error,'-o',label='Monte-Carlo')
loglog([1,1e7],[1,sqrt(1e-7)],'k',label='1 / sqrt(N)')
legend()
xlabel('number of random walks used')
ylabel('abs(error)')
title('Log-log plot of relative error in MC Laplace')
savefig('mc_laplace_error.png')
