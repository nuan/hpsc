! Quadrature module

module quadrature

contains

real(kind = 8) function trapezoid(f, a, b, n)
	implicit none
	real(kind = 8), external :: f
	real(kind = 8), intent(in) :: a, b
	integer, intent(in) :: n

	real(kind = 8) :: h, sum
	real(kind = 8), dimension(n) :: xj, fj
	integer :: i

	h = (b-a)/(n-1)

	!allocate(xj(n))
	!allocate(fj(n))

	xj(1) = a
	fj(1) = f(a)
	sum = fj(1)
	do i = 2, n
		xj(i) = xj(i-1) + h
		fj(i) = f(xj(i))
		sum = sum + fj(i)
		end do

	trapezoid = h * sum - 0.5*h*(fj(1) + fj(n))

	!deallocate(xj)
	!deallocate(fj)
end function trapezoid

subroutine error_table(f, a, b, nvals, int_true)
	implicit none
	real(kind = 8), external :: f
	real(kind = 8), intent(in) :: a, b, int_true
	integer, dimension(:), intent(in) :: nvals

	real(kind = 8) :: last_error = 0, int_trap, error, ratio
	integer :: i, n

	print *, "    n         trapezoid            error       ratio"

	do i = 1, size(nvals)
		n = nvals(i)
		int_trap = trapezoid(f, a, b, n)
		error = abs(int_trap - int_true)
		ratio = last_error/error
		last_error = error

		print 11, n, int_trap, error, ratio
	11	format(i8, es22.14, es13.3, es13.3)
		end do

	
end subroutine error_table

end module quadrature
